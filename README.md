## NYC High School App : Introduction ##
This is that a simple application to display NYC Schools List and their SAT Scores.

- **iOS** support
- Swift 5.0
- Xocde 10.3

### Data Flow ###
- This application was built with VIPER Architecture pattern.
- All classes were designed for testable and independent.


![ScreenShot](./Docs/AppDataFlow.png)


