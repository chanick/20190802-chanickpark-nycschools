//
//  MockSchoolSATNetworkManager.swift
//  20190802-ChanickPark-NYCSchoolsTests
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import XCTest
@testable import _0190802_ChanickPark_NYCSchools



//
// MockSchoolNetworkManager
//
struct MockSchoolSATNetworkManager: NetworkManager {
    var client: NetworkClient
    var relativePath: String
    
    init(client: NetworkClient) {
        self.client = client
        self.relativePath = "NYCHighSchoolsSAT"    // Schools SAT json file name
    }
    
    /**
     * @desc Request Schools List from local Json file
     */
    func requestSchoolSAT(dbn: String, callback: ((NetworkClientResult<[SchoolSAT]>) -> Void)?) {
        client.request(method: .get, path: path, parameters: nil, httpBody: nil, callback: { (result: NetworkClientResult<[SchoolSAT]>) in
            callback?(result)
        })
    }
}
