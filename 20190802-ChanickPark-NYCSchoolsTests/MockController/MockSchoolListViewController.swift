//
//  MockSchoolListViewController.swift
//  20190802-ChanickPark-NYCSchoolsTests
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import Foundation


import XCTest
@testable import _0190802_ChanickPark_NYCSchools

// Create Mock Class
class MockSchoolListViewController: XCTestCase {
    // Create school network manager with mock school network client
    var schoolNetworkManager = MockSchoolNetworkManager(client: MockSchoolNetworkClient())
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchSchoolList() throws {
        // Request Schools List (load from local json file)
        schoolNetworkManager.requestSchoolsList { (result) in
            switch result {
            case .success(let schools):
                XCTAssertEqual(schools.count, 440)
                
            case .failure(let errors):
                XCTFail(errors.first?.description ?? "")
            }
        }
    }
}
