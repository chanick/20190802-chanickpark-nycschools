//
//  ArrayExtension.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import Foundation


extension Array {
    /**
     * @desc safe accee with index
     * @param index
     * @return Element optional
     */
    subscript (safe index: Index) -> Element? {
        return index < count ? self[index] : nil
    }
}
