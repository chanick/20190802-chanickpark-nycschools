//
//  Dictionary+Extension.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import Foundation


extension Dictionary {
    /**
     * @desc Update value in Immutable dictionary
     * @param [Key:Value]
     */
    mutating func add(dictionary: [Key: Value]) {
        dictionary.forEach { (key, value) in
            self.updateValue(value, forKey: key)
        }
    }
}
