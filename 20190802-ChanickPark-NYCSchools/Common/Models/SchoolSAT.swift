//
//  SchoolSAT.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import Foundation


//
// School SAT Model
//
struct SchoolSAT : Codable {
    let dbn: String
    let school_name: String
    let num_of_sat_test_takers: String
    let sat_critical_reading_avg_score: String
    let sat_math_avg_score: String
    let sat_writing_avg_score: String
    
    // Displays List
    var displays: [SchoolSATItem] {
        return [SchoolSATItem(title: "Num of SAT Test Takers", score: num_of_sat_test_takers),
                SchoolSATItem(title: "SAT Critical Reading Avg. Score", score: sat_critical_reading_avg_score),
                SchoolSATItem(title: "SAT Math Avg. Score", score: sat_math_avg_score),
                SchoolSATItem(title: "SAT Writing Avg. Score", score: sat_writing_avg_score)]
    }
}

struct SchoolSATItem : Codable {
    var title: String = ""
    var score: String = ""
}

extension SchoolSATItem : SchoolSATItemInterface {
    var itemTitle: String? {
        return title
    }
    
    var itemScore: String? {
        return score
    }
    
}

