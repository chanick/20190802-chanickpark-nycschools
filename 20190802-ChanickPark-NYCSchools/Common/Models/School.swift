//
//  School.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import UIKit

//
// School Model
//
struct School : Codable {
    var dbn: String = ""
    var school_name: String = ""
    var city: String = ""
}

extension School : HomeViewItemInterface {
    var schoolName: String? {
        return school_name
    }
    var cityName: String? {
        return city
    }
}
