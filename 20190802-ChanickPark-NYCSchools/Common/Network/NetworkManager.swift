//
//  NetworkManager.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import Foundation

//
// NetworkManager protocol
//
protocol NetworkManager {
    var client: NetworkClient { get }
    var relativePath: String { get }
    var path: String { get }
}

extension NetworkManager {
    var path: String {
        return "\(client.basePath)\(relativePath)"
    }
}
