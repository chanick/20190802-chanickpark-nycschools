//
//  DetailPresenter.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by OGiP on 8/2/19.
//  Copyright (c) 2019 Chanick Park. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class DetailPresenter {

    // MARK: - Private properties -

    private unowned let view: DetailViewInterface
    private let interactor: DetailInteractorInterface
    private let wireframe: DetailWireframeInterface

    private let school: School
    var schoolSATItem: [SchoolSATItem] = [] {
        didSet {
            view.reloadData()
        }
    }
    
    // MARK: - Lifecycle -

    init(view: DetailViewInterface, interactor: DetailInteractorInterface, wireframe: DetailWireframeInterface, school: School) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.school = school
    }
}

// MARK: - Extensions -

extension DetailPresenter: DetailPresenterInterface {
    func numberOrItems() -> Int {
        return schoolSATItem.count
    }
    
    func item(at indexPath: IndexPath) -> SchoolSATItemInterface {
        return schoolSATItem[safe: indexPath.row] ?? SchoolSATItem()
    }
    
    func viewDidLoad() {
        view.setViewTitle(school.school_name)
        
        self.view.setLoadingVisible(true)
        interactor.getSchoolSAT(dbn: self.school.dbn) { [weak self] result in
            self?.view.setLoadingVisible(false)
            self?.handleSchoolSATResult(result)
        }
    }
    
    // MARK: Utility
    
    private func handleSchoolSATResult(_ result: NetworkClientResult<[SchoolSAT]>) {
        switch result {
        case .success(let schoolSAT):
            if let schoolSAT = schoolSAT.first {
                self.schoolSATItem = schoolSAT.displays
            }
            
        case .failure(let error):
            wireframe.showErrorAlert(with: error.first.debugDescription)
        }
    }
}
