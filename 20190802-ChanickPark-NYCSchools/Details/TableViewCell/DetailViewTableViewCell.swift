//
//  DetailViewTableViewCell.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by Park, Chanick on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import UIKit

//
// DetailViewTableViewCell
//
class DetailViewTableViewCell : UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var scoreLbl: UILabel!
    
    
    func configure(with item: SchoolSATItemInterface) {
        titleLbl.text = item.itemTitle
        scoreLbl.text = item.itemScore
    }
}
