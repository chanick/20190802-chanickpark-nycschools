//
//  HomeViewTableViewCell.swift
//  20190802-ChanickPark-NYCSchools
//
//  Created by chanick park on 8/2/19.
//  Copyright © 2019 Chanick Park. All rights reserved.
//

import UIKit

//
// HomeViewTableViewCell
//
class HomeViewTableViewCell : UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    
    func configure(with item: HomeViewItemInterface) {
        nameLbl.text = item.schoolName
    }
}
